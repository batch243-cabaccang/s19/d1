let a = -1;

if (a < 0) {
  console.log(true);
}

let city = "New York";

if (city === "New York") {
  console.log("You're in New York");
} else {
  console.log("Where are you?");
}

city = "Tokyo";

if (city === "New York") {
  console.log("Your in New York");
} else if (city === "Tokyo") {
  console.log("Tokyo, baby!");
} else {
  console.log("Huh?");
}

let message;

const typhoonIntensity = (ws) => {
  if (ws < 0) {
    return "Invalid Argument";
  } else if (ws > 0 && ws <= 30) {
    return "Not a typhoon yet";
  } else if (ws >= 31 && ws <= 60) {
    return "Tropical Depression Detected";
  } else if (ws >= 61 && ws <= 88) {
    return "Tropical Storm Detected";
  } else if (ws >= 89 && ws <= 117) {
    return "Not a typhoon yet";
  } else {
    return "Typhoon Detected";
  }
};

message = typhoonIntensity(120);
console.warn(message);

// the one line if-else statment is call TERNARY!! DO NOT FORGET!!

let firstName;

const isOfLegalAge = () => {
  firstName = "John";
  return "You are of legal age";
};

const isNotLegalAge = () => {
  firstName = "Jane";
  return "You are not of legal age";
};

let age = 20;
let legalAge = age > 18 ? isOfLegalAge() : isNotLegalAge();
console.log(legalAge, firstName);

//
let day = prompt("What the day today?").toLowerCase();

switch (day) {
  case "monday":
    console.log("Today's monday");
    break;
  case "tuesday":
    console.log("Today's tuesday");
    break;
  case "wednesday":
    console.log("Today's wednesday");
    break;
  case "thursday":
    console.log("Today's thursday");
    break;
  case "friday":
    console.log("Today's friday");
    break;
  case "saturday":
    console.log("Today's saturday");
    break;
  case "sunday":
    console.log("Today's sunday");
    break;
  default:
    console.log("Input is not a day");
}

// try-catch-finally

const showError = () => {
  try {
    alerta();
  } catch (error) {
    console.log(error.message);
  } finally {
    console.log("fix your code");
  }
};

showError();
console.log("okay?")
